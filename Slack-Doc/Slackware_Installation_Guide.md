## Slackware 15 Current Installation BTRFS Filesystem LUKS, With Swapfile 
---

**Datum : September 2, 2022 Tag: Freitag**

## **References**
---

    - https://gitlab.com/slackernetuk/slack-desk/-/blob/master/docs/luks-full.txt             
    - https://mwendi.wordpress.com/2011/04/30/install-slackware-di-btrfs-filesystem/ 
    - https://btrfs.readthedocs.io/en/latest/Swapfile.html 
    - https://docs.slackware.com/howtos:slackware_admin:how_to_chroot_from_media#volume_mounting 
    - https://www.vivaolinux.com.br/dica/Instalando-Slackware-com-btrfs 
    - https://github.com/scardracs/genfstab 

## **Set Keyboard Layout**

If using a different keyboard layout from the standard us layout follow the on screen options. 

## **Set Password(Optionally)**

Changed the default root password if you wish to remote login. (eg: ssh) 

'''

passwd 

'''

## **Connect to the Internet** 

Use this command to gather information about your ethernet or wlan connection : 

'''

ifconfig -a 

'''


If usings a ethernet connect : 

'''
dhcpcd eth0 
'''

If using [wifi connection](https://wiki.alienbase.nl/doku.php?id=slackware:network#wireless_networks) : 



## **Start SSH Service in Slackware**


'''

sh /etc/rc.d/rc.dropbear start 

'''

## **Connect by to the ssh service**

Replace the variable "username" with your user for the ssh connection , replace "ip-address-of-machine" with the ip address of your machine. 

'''

ssh username@ip-address-of-machine 

'''

## **Partition the disks**

USE either fdisk or cfdisk to partion the disk. 
Use this command to list the disk in your system. 

'''

fdisk -l 

'''

Output 

> Disk /dev/vda: 40 GiB, 42949672960 bytes, 83886080 sectors
> Units: sectors of 1 * 512 = 512 bytes
> Sector size (logical/physical): 512 bytes / 512 bytes
> I/O size (minimum/optimal): 512 bytes / 512 bytes


Lets begin the partition , I will be using "sgdisk" if you want a graphically display use either "cfdisk" or "cgdisk" 
- EFISYSTEM : ef00 
- BIOS BOOT PARTITION : ef02 
- Linux Filesystem : 8300
'''

sgdisk 

    -n1:0:+512M  -t1:ef00 -c1:EFISYSTEM 

    -n2:0:+1.5G -t2:ef02 -c2:BOOT 

    -N3          -t3:8300 -c3:root 

    /dev/vda 

'''

List the Partition table using 

"fdisk -l"

Output 

> Device       Start      End  Sectors  Size Type
> /dev/vda1     2048  1050623  1048576  512M EFI System
> /dev/vda2  1050624  4196351  3145728  1.5G BIOS boot
> /dev/vda3  4196352 83884031 79687680   38G Linux filesystem

## **Creating the Encryption**

'''

cryptsetup -s 512 luksFormat --type luks1 /dev/vda3  

cryptsetup luksOpen /dev/vda3  cryptroot 

'''

- Format the partition and Mount the root device  

'''

mkfs.vfat  /dev/vda1  

mkfs.btrfs -L root -f -n 32k  /dev/mapper/cryptroot 

mount -t btrfs /dev/mapper/cryptroot   /mnt 

'''

- Creating the BTRFS SUBVOLUMES 

'''

btrfs subvolume create /mnt/@

btrfs subvolume create /mnt/@home

btrfs subvolume create /mnt/@boot 

umount /mnt 

'''

- Mount the Subvolumes 

'''

mount -o noatime,ssd,commit=120,compress=zstd,space_cache=v2,discard=async,subvol=@  -t btrfs /dev/mapper/cryptroot /mnt 

mkdir -p /mnt/{home,/boot/efi}

mount -o noatime,ssd,commit=120,compress=zstd,space_cache=v2,discard=async,subvol=@home  -t btrfs /dev/mapper/cryptroot  /mnt/home 

mount /dev/vda1  /mnt/boot/efi 

'''

To begin the installer type the command : 

'''

setup 

'''

- Ensure to Skip install LILO and ELILO , we will be installing [Grub:GrandUnified Bootloader](https://wiki.archlinux.org/title/GRUB) 



## **Post-Installation**

Exit the installer and select the option for the shell. Do NOT REBOOT AT THIS TIME. 
We will fix your bootloader and other essential step at this stage.(Add Users)
Chroot into the system 

'''

cp -R /etc/resolv.conf /mnt/etc/resolv.conf

chroot /mnt /bin/bash -l

'''

- Edit the Filesystem Table to include the btrfs subvolume 
- Using Genfstab script 

'''

mkdir /tmp/genfstab-build

cd /tmp/genfstab-build

git clone https://github.com/scardracs/genfstab.git 

cd genfstab 

make 

'''

- Generate the Filesystem Table 

'''

./genfstab -U / >> /etc/fstab 

'''

- Adjust the /etc/fstab file 

'''

vim /etc/fstab 

'''

- Creating the BTRFS SWAP FILE of size 2GB 

'''

truncate -s 0 /swapfile 

chattr +C /swapfile

fallocate -l 2G /swapfile

chmod 0600 /swapfile

mkswap /swapfile

swapon /swapfile 

'''

- Add the swapfile to the Filesystem Table 

'''

echo "#Swapfile" >> /etc/fstab 

echo "/swapfile    none     swap   defaults   0   0" >> /etc/fstab

'''

- Creating the Keyfile this is to help with only enter your password for the root partition at grub. Normally without this you enter your passphrase twice, one for the boot and another the root paritition. 

'''

dd bs=512 count=4 if=/dev/urandom of=/slackpv.keyfile

cryptsetup luksAddKey /dev/vda3 /slackpv.keyfile 

chmod 000 /slackpv.keyfile 

'''

- Initial Settings for the Initial Ram disk (initrd) , Notice credits goes to [Frank Honolka](https://gitlab.com/slackernetuk) for creating this patch for grub. 
- Reference : https://gitlab.com/slackernetuk/slack-desk/-/blob/master/docs/luks-full.txt 

'''

mkdir /tmp/initrd-tree 

tar xpzf /usr/share/mkinitrd/initrd-tree.tar.gz -C /tmp/initrd-tree 

'''

'''

cd /tmp/initrd-tree
wget https://gitlab.com/slackernetuk/slack-desk/-/raw/master/patches/key_file_in_the_initrd_and_drive_unlocked_by_grub.diff


patch init < key_file_in_the_initrd_and_drive_unlocked_by_grub.diff 
mv /slackpv.keyfile ./ 

tar cpzf /usr/share/mkinitrd/initrd-tree.tar.gz *


'''

- A mkinitrd helper script 

This exist in slackware 14.0 and up , therefore you should be able to use this script to assist you in setting the mkinitrd. 

'''

/usr/share/mkinitrd/mkinitrd_command_generator.sh -r 

'''
Output 

'''

mkinitrd -c -k 5.19.6 -f btrfs -r cryptroot -m virtio_blk:virtio_pci_modern_dev:virtio_pci_legacy_dev:virtio_pci:virtio:virtio_balloon:virtio_ring:virtio_net:zstd_compress:blake2b_generic:btrfs -C /dev/vda3  -u -o /boot/initrd.gz 


'''

- Use the above command and modify it. Add the keyfile options to command , if you created a keyfile earlier during the installation. 

'''

mkinitrd -c  -k 5.19.6 -f btrfs -r /dev/mapper/cryptroot -m virtio_blk:virtio_pci_modern_dev:virtio_pci_legacy_dev:virtio_pci:virtio:virtio_balloon:virtio_ring:virtio_net:zstd_compress:blake2b_generic:btrfs -C /dev/vda3 -L -K /slackpv.keyfile -u -o /boot/initrd.gz 

'''


- Adjusting the init script to boot from btrfs subvolume 
- Edit the file /boot/initrd-tree/init. Search the $ROOTFS section and adjust according 

'''

mount -o ro -t $ROOTFS $ROOTDEV /mnt 

'''

'''

mount -o ro,noatime,ssd,commit=120,compress=zstd,space_cache=v2,discard=async,subvol=/@ -t $ROOTFS $ROOTDEV /mnt

'''

- Steps to create the new initrd.gz 

'''

cd /boot 

rm initrd.gz 

'''

- Use the command above but adjust to include the keyfile and your ROOT DEVICE 
- Now run mkinitrd again without the -c option (this option is only to refresh the initrd-tree), the details are: 

'''

mkinitrd -k 5.19.6 -f btrfs -r /dev/mapper/cryptroot -m virtio_blk:virtio_pci_modern_dev:virtio_pci_legacy_dev:virtio_pci:virtio:virtio_balloon:virtio_ring:virtio_net:zstd_compress:blake2b_generic:btrfs -C /dev/vda3 -L -K /slackpv.keyfile -u -o /boot/initrd.gz 

'''

- Edit the /etc/default/grub 

'''

vim /etc/default/grub 

'''

'''

GRUB_CMDLINE_LINUX="cryptdevice=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxx:cryptroot root=/dev/mapper/cryptroot cryptkey=/slackpv.keyfile"
GRUB_ENABLE_CRYPTODISK=y

'''

- Adjust the timeout section (Optionally Step)

'''

GRUB_TIMEOUT=15 

'''

- Install the Grub bootloader and regenerate the grub configuration file 

'''

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=SLACKWARE-BTRFS-LUKS --recheck 

grub-mkconfig -o /boot/grub/grub.cfg

'''

- Creating a New user 

'''

useradd -m -g users -G wheel,floppy,audio,video,cdrom,plugdev,power,netdev,lp,scanner -s /bin/bash slacker 

'''

- Create a Password for the user 

'''

passwd slacker 

'''

- Give the user sudo privilages 

'''

EDITOR=vim  visudo 

'''

- Uncomment the line to allow any members of the wheel group 

'''

\## Uncomment to allow members of group wheel to execute any command

\#%wheel ALL=(ALL:ALL) ALL

'''

'''

\## Uncomment to allow members of group wheel to execute any command

%wheel ALL=(ALL:ALL) ALL

'''

- Reboot the machine 

'''

reboot 

'''

## **After Installation **

- Login with your username and password. 
- Update the system and Regenerate the File System Table 

'''

cd /tmp/genfstab-build/genfstab/ 

./genfstab -U / >> /etc/fstab 

'''

- Update the Grub Configuration file
- Adjust the File System Table before update the grub configuration 
- For reference , please see the [youtube video](https://www.youtube.com/watch?v=1g4cC8eSMHg)

'''

vim /etc/fstab 

grub-mkconfig -o /boot/grub/grub.cfg 

'''

- Updating the system 
- Uncomment one of the mirrors before you update the system 
- Notice each command below needs "sudo" or "root" privilages 
'''

vim /etc/slackpkg/mirrors 

slackpkg update gpg 

slackpkg update 

slackpkg install-new
 
'''


## **Troubleshooting**
---

- If something went wrong after the installation you can Chroot into the existing system to attempt fix your problem. 

'''

cryptsetup open /dev/vda3 cryptroot 

mount -o noatime,ssd,commit=120,compress=zstd,space_cache=v2,discard=async,subvol=@  -t btrfs /dev/mapper/cryptroot /mnt 

mount -o noatime,ssd,commit=120,compress=zstd,space_cache=v2,discard=async,subvol=@home  -t btrfs /dev/mapper/cryptroot /mnt/home

mount /dev/vda1   /mnt/boot/efi 

mount -o bind /dev   /mnt/dev 

mount -o bind /proc  /mnt/proc 

mount -o bind /sys   /mnt/sys 

cd /mnt 

chroot /mnt  /bin/bash -l 

'''
